import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  constructor(
    private diagnostic: Diagnostic,
    private openNativeSettings: OpenNativeSettings,
    private router: Router,
  ) {
    setTimeout(() => {
      //check location permissions if not get permissions
      this.requestLocationPermissions();
      
      this.watchLocationStatus();
    }, 500);
  }

  async requestLocationPermissions() {
    try {
      const auth = await this.diagnostic.requestLocationAuthorization();
      switch(auth) {
        case 'DENIED_ONCE': this.requestLocationPermissions(); break;
        case 'DENIED_ALWAYS': alert('Доступ к геолокации запрещён'); break;
        case 'authorized_when_in_use': this.checkLocation();
      }
    } catch(err) {
      alert(`Err ${err}`);
    }
  }

  async checkLocation() {    
    try {
      const mode = await this.diagnostic.getLocationMode();
      this.handleLocationStatus(mode);
    } catch(err) {
      alert(`Err ${err}`);
    }
  }

  handleLocationStatus(status: string) {
    if (status === 'location_off') {
      // alert(`GPS disabled`);
      this.router.navigate(['/geo'], { replaceUrl: true });
    } else {
      // alert(`GPS enabled`);
      this.router.navigate(['/public'], { replaceUrl: true });
    }
  }

  enableLocation() {
    this.openNativeSettings.open('location');
  }

  watchLocationStatus() {
    this.diagnostic.registerLocationStateChangeHandler((accuracyAuthorization) => {
      this.handleLocationStatus(accuracyAuthorization);
    })
  }
}
