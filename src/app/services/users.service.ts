import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient,
  ) { }

  url = `https://jsonplaceholder.typicode.com/users`;

  getUsers(): Observable<User[]> {
    return this.http.get<any[]>(this.url);
  }
}

