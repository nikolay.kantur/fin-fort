import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.page.html',
  styleUrls: ['./public.page.scss'],
})
export class PublicPage implements OnInit {

  users: User[] = [];

  constructor(
    private userService: UsersService
  ) {}

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  delete(id) {
    this.users = this.users.filter(user => user.id !== id);
  }

}
