import { Component, OnInit } from '@angular/core';
import { GeoService } from 'src/app/services/geo.service';

@Component({
  selector: 'app-geo',
  templateUrl: './geo.page.html',
  styleUrls: ['./geo.page.scss'],
})
export class GeoPage implements OnInit {

  constructor(
    public geoService: GeoService,
  ) { }

  ngOnInit() {
  }

}
